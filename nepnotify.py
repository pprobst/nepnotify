#!/usr/bin/env python3
import random
import gi
gi.require_version('Notify', '0.7')
from gi.repository import Notify, GdkPixbuf
from apscheduler.schedulers.blocking import BlockingScheduler

class Nepnotify:
    def __init__(self):
        Notify.init("Nepnotify")

    def send(self, title, text, image_path):
        n = Notify.Notification.new(title, text)
        image = GdkPixbuf.Pixbuf.new_from_file(image_path)
        n.set_image_from_pixbuf(image)
        n.show()

note = Nepnotify()

sched = BlockingScheduler()

@sched.scheduled_job('interval', hours=1)
def send_notification():
    nepnep = "Nepu! It's time to drink some water~!"
    nepnep2 = "\"Leave for tomorrow what you can do today\", as they say."
    noire = "H-hey, you should drink some water now... D-don't get me wrong, it's not like I'm worried or anything!"
    noire2 = "Don't forget your responsibilities for the day."
    blanc = "Drink water. It's good for you."
    blanc2 = "Shall we read a book together?"
    vert = "Ara, ara. Looks like someone needs to drink water. Are you reading lewd doujinshi again?"
    vert2 = "Why are you looking at the screen so intently?"
    plutia = "You shooould drink water or I'll get reeeeaaally mad~"
    plutia2 = "Heeeey, stop everything and let's naaaap."
    iris_heart = "Huh? Why should I let scum like you drink water?"
    iris_heart2 = "Stop eating, pig."

    text_list = [nepnep, nepnep2, noire, noire2, blanc, blanc2, vert, vert2,
             plutia, plutia2, iris_heart, iris_heart2]

    mode = random.choice(text_list)

    if mode == nepnep or mode == nepnep2:
        title = "Neptunia"
        image = "neptunia.png"

    elif mode == noire or mode == noire2:
        title = "Noire"
        image = "noire.png"

    elif mode == blanc or mode == blanc2:
        title = "Blanc"
        image = "blanc.png"

    elif mode == vert or mode == vert2:
        title = "Vert"
        image = "vert.png"

    elif mode == plutia or mode == plutia2:
        title = "Plutia"
        image = "plutia.png"

    elif mode == iris_heart or mode == iris_heart2:
        title = "Iris Heart"
        image = "iris_heart.png"
    
    note.send(title, mode, image)

sched.start()
