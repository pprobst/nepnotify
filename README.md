# nepnotify
Small program that reminds you of "important" stuff using cute 2D girls from the Hyperdimension Neptunia franchise.

## Dependencies

* [Python 3](https://www.python.org/)
* [libnotify](https://developer.gnome.org/libnotify/)
* [python-gobject](https://wiki.gnome.org/action/show/Projects/PyGObject?action=show&redirect=PyGObject)
* [apscheduler](https://github.com/agronholm/apscheduler)

You'll also need a notification daemon with .png support to display the portraits.

## How to use
Just set it up to run in the background with the method you like the most and you're done.
You'll receive the notifications hourly, but you can change the interval in line 22 of [nepnotify.py](https://github.com/pprobst/nepnotify/blob/master/nepnotify.py#L22).

## Disclaimer
This program is not related to [Idea Factory](http://www.ideaf.co.jp/), [Compile Heart](http://www.compileheart.com/), [Felistella](http://felistella.co.jp/) or any of its associate partners. As the portraits are not my property, I'll simply avoid adding a license; however, if requested by the owner, I'll remove the portraits. 
Anyone is free to modify the code.
